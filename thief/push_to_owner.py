import os
from os.path import isfile, join
from git import Repo, InvalidGitRepositoryError
from dotenv import load_dotenv
from github import Github

load_dotenv()
token = os.getenv("destination_password")
local_path = os.getenv("local_path")


def main():
    username_github = os.getenv('destination_username')
    gh = Github(token)
    user = gh.get_user()
    repos = [x.name for x in list(user.get_repos()) if x.full_name.startswith(username_github)]

    list_loca_repos = []
    for _name in os.getenv("source_victim_name").split(","):
        folder_path = local_path + "/" + _name
        list_loca_repos += [(f, _name, folder_path + "/" + f) for f in os.listdir(folder_path) if not isfile(join(folder_path, f))]

    for name_repo, name_group, repo_local_path in list_loca_repos:
        github_repo_name = f"{name_group}_{name_repo}"
        print(github_repo_name)
        try:
            repo = Repo(repo_local_path)
        except InvalidGitRepositoryError as invalid_error:
            print(invalid_error)
            list_loca_repos += [(f, github_repo_name, repo_local_path + "/" + f) for f in os.listdir(repo_local_path) if
                                not isfile(join(repo_local_path, f))]
            continue

        old_origin_url = repo.remotes.origin.url
        print(repo_local_path)
        remote_url_origin = f"git@github.com:{username_github}/{github_repo_name}.git"
        print(remote_url_origin)

        if github_repo_name not in repos:
            user.create_repo(name=github_repo_name, private=True)

        repo.remotes.origin.set_url(remote_url_origin)
        repo.remote(name='origin').push(all=True)
        repo.remotes.origin.set_url(old_origin_url)


if __name__ == '__main__':
    main()
