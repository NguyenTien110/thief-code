import logging
import os
import subprocess
from typing import List

import gitlab
from dotenv import load_dotenv
from git import Repo, NoSuchPathError
from gitlab.v4.objects import ProjectBranch
from gitlab.v4.objects.projects import GroupProject

from service import service_gitlab

load_dotenv()
local_path = os.getenv("local_path")

if not os.path.exists(local_path):
    raise Exception(f"local_path does not exist: {os.getenv('local_path')}")


def get_list_project_gitlab(list_victim: List[str]):
    _ret = []
    gl = service_gitlab.create_gitlab_connection(dict(os.environ))

    # get all list projects in groups
    if os.getenv('source_victim_type') == 'groups':
        all_groups: List[gitlab.v4.objects.groups.Group] = gl.groups.list(get_all=True)
        for gr in all_groups:
            if gr.path.lower() in list_victim:
                print("crawl", gr.asdict().get('web_url'), gr.asdict().get('name'))
                prs = gl.groups.get(id=gr.id).projects
                _list_project: List[GroupProject] = prs.list(include_subgroups=True, get_all=True)
                for _prj in _list_project:
                    print("get prj", _prj.asdict().get('web_url'))
                    try:
                        _branches: List[ProjectBranch] = gl.projects.get(id=_prj.get_id(),
                                                                         statistics=True).branches.list(get_all=True)
                        if _branches:
                            _sorted_branched = sorted(_branches, key=lambda x: x.asdict()["commit"]["committed_date"])
                            _full_prj = gl.projects.get(id=_prj.get_id(), statistics=True).asdict()
                            _full_prj["newest_branch"] = _sorted_branched[-1].asdict().get("name")
                            _ret.append(_full_prj)
                    except Exception as _error:
                        logging.error(str(_error) + " " + _prj.to_json())
                        raise
    # get all list projects of users
    elif os.getenv('source_victim_type') == 'users':
        for _u in list_victim:
            _user = gl.users.list(username=_u)
            if _user:
                _user = _user[0]
                _list_project: List[GroupProject] = _user.projects.list(get_all=True)
                for _prj in _list_project:
                    _branches: List[ProjectBranch] = gl.projects.get(id=_prj.get_id(), statistics=True).branches.list(
                        get_all=True)
                    _sorted_branched = sorted(_branches, key=lambda x: x.asdict()["commit"]["committed_date"])
                    _full_prj = gl.projects.get(id=_prj.get_id(), statistics=True).asdict()
                    _full_prj["newest_branch"] = _sorted_branched[-1].asdict().get("name")
                    _ret.append(_full_prj)
    return _ret


def get_list_project() -> List[dict]:
    _ret: List[dict] = []
    list_victim = os.getenv("source_victim_name").split(",")
    if os.getenv("source_git_type") == "gitlab":
        _ret = get_list_project_gitlab(list_victim=list_victim)
    # todo
    elif os.getenv("source_git_type") == "github":
        raise Exception("not supported")
    elif os.getenv("source_git_type") == "bitbucket":
        raise Exception("not supported")
    else:
        raise Exception(f"fail to load git type {os.getenv('source_git_type')}")

    return _ret


def clone_project(_prj: dict):
    print("clone_project " + _prj.get("path"))
    path_folder = f"{local_path}/{_prj.get('namespace').get('full_path')}"
    if not os.path.exists(path_folder):
        os.mkdir(path_folder)

    repo = Repo.clone_from(_prj.get("http_url_to_repo"), path_folder + '/' + _prj.get("path"))
    print('git clone', _prj.get("http_url_to_repo"), repo)
    repo.git.checkout(_prj.get("newest_branch"))
    print('git checkout', _prj.get("newest_branch"))
    repo.remotes.origin.pull()
    print("====================================")


def pull_project(_prj: dict):
    print("pull project " + _prj.get("path"))
    _path_folder = f"{local_path}/{_prj.get('namespace').get('full_path')}/{_prj.get('path')}"
    try:
        repo = Repo(_path_folder)
        if _prj.get("http_url_to_repo") != repo.remotes.origin.url:
            _result = subprocess.run(['rm', '-rf', _path_folder], stdout=subprocess.PIPE)
            clone_project(_prj)
        else:
            repo.git.checkout(_prj.get("newest_branch"))
            print('git checkout', _prj.get("newest_branch"))
            repo.remotes.origin.pull()
            print('git pull')
        print("====================================")
    except NoSuchPathError as not_found_error:
        print("not found", not_found_error)
        clone_project(_prj)


def download_project(_list_project: List[dict]) -> None:
    for _prj in _list_project:
        pull_project(_prj)


def main():
    projects = get_list_project()
    print(len(projects))
    print(list(map(lambda x: x['path'], projects)))

    download_project(projects)


if __name__ == '__main__':
    main()
