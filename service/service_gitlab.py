import gitlab


def create_gitlab_connection(env: dict):
    _http_username = env.get("source_credential_username")
    _http_password = env.get("source_credential_password")
    _access_token = env.get("source_access_token")
    _source_path = env.get("source_path")
    _info = dict(url=_source_path, api_version='4', keep_base_url=True)
    if _access_token is not None:
        _info['private_token'] = _access_token
    elif _http_username is not None and _http_password is not None:
        _info['http_username'] = _http_username
        _info['http_password'] = _http_password
    else:
        raise Exception("missing credentials for gitlab")

    return gitlab.Gitlab(**_info)


if __name__ == '__main__':
    from dotenv import load_dotenv
    import os

    load_dotenv()
    _gl = create_gitlab_connection(dict(os.environ))
    all_groups = _gl.groups.list(get_all=True)
    print(all_groups)
